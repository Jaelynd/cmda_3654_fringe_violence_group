# -*- coding: utf-8 -*-
"""
Created on Sat Nov 24 21:47:11 2018

@author: Wylans xps 13
"""

import pandas as pd
import matplotlib.pyplot as plt
file = None
with open("./globalterrorism2.csv") as file:
    file = pd.read_csv(file)



#Jewish groups
new = file[["gname", "year", "state"]]
new = new[new["year"] > 1980]


years = jews["year"].unique()
years = range(min(jews["year"]), max(jews["year"]) + 1)
counts = []
for x in years:
    summer = sum(jews["year"] == x)
    counts.append(summer)
plt.plot(years, counts)
plt.xlabel("Year")
plt.ylabel("Number of Violence Events")
plt.title("Jewish Nationalist Violence Events")
plt.savefig('./Jewish_Nationalist_Violence.png', format='png', dpi=1000)
plt.show()




#Black Groups

years = blacks["year"].unique()
years = range(min(blacks["year"]), max(blacks["year"]) + 1)
counts = []
for x in years:
    summer = sum(blacks["year"] == x)
    counts.append(summer)
plt.plot(years, counts)
plt.title("Black Nationalist Violence Events")
plt.xlabel("Year")
plt.ylabel("Number of Violence Events")
plt.savefig('./Black_Nationalist_Violence.png', format='png', dpi=1000)

plt.show()



#White groups


years = whites["year"].unique()
years = range(min(whites["year"]), max(whites["year"]) + 1)
counts = []
for x in years:
    summer = sum(whites["year"] == x)
    counts.append(summer)
plt.plot(years, counts)
plt.title("White Nationalist Violence Events")
plt.xlabel("Year")
plt.ylabel("Number of Violence Events")
plt.savefig('./White_Nationalist_Violence.png', format='png', dpi=1000)

plt.show()
