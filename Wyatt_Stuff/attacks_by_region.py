# -*- coding: utf-8 -*-
"""
Created on Sun Dec  2 14:16:57 2018

@author: Wylans xps 13
"""
import pandas as pd
import matplotlib.pyplot as plt
divisions = {"New England" : ["Connecticut", "Maine", "Massachusetts", "New Hampshire", "Rhode Island", "Vermont"], "Mid-Atlantic" : ["New Jersey", "New York", "Pennsylvania", "Puerto Rico"], "East North Central" : ["Illinois", "Indiana", "Michigan", "Ohio", "Wisconsin"], "West North Central" : ["Iowa", "Kansas", "Minnesota", "Missouri", "Nebraska", "North Dakota", "South Dakota"], "South Atlantic" : ["Delaware", "Florida", "Georgia", "Maryland", "North Carolina", "South Carolina", "Virginia", "Washington D.C.", "West Virginia"], "East South Central" : ["Alabama", "Kentucky", "Mississippi", "Tennessee"], "West South Central" : ["Arkansas", "Louisiana", "Oklahoma", "Texas"], "Mountain" : ["Arizona", "Colorado", "Idaho", "Montana", "Nevada", "New Mexico", "Utah", "Wyoming"], "Pacific" : ["Alaska", "California", "Hawaii", "Oregon", "Washington"]}

file = None
with open("./globalterrorism.csv") as file:
    file = pd.read_csv(file)



#Jewish groups
new = file[["gname", "year", "state"]]
jews = new[new["gname"].str.contains("Jew")]

jews_by_region = {"New England" : 0, "Mid-Atlantic" : 0, "East North Central" : 0, "West North Central" : 0, "South Atlantic" : 0, "East South Central" :0, "West South Central" : 0, "Mountain" : 0, "Pacific" : 0}
for key, values in divisions.items():
    for value in values: 
        for attack_state in jews["state"]:
            if value == attack_state:
                jews_by_region[key] += 1

                

#Black Groups


blacks = new[new["gname"].str.contains("Black") | new["gname"].str.contains("African") | new["gname"].str.contains("Anti-W")]
    
blacks_by_region = {"New England" : 0, "Mid-Atlantic" : 0, "East North Central" : 0, "West North Central" : 0, "South Atlantic" : 0, "East South Central" :0, "West South Central" : 0, "Mountain" : 0, "Pacific" : 0}
for key, values in divisions.items():
    for value in values: 
        for attack_state in blacks["state"]:
            if value == attack_state:
                blacks_by_region[key] += 1



#White groups
whites = new[new["gname"].str.contains("White") | new["gname"].str.contains("Ku") | new["gname"].str.contains("Nazi") | new["gname"].str.contains("Aryan") & ~new["gname"].str.contains("Anti-White")]

whites_by_region = {"New England" : 0, "Mid-Atlantic" : 0, "East North Central" : 0, "West North Central" : 0, "South Atlantic" : 0, "East South Central" :0, "West South Central" : 0, "Mountain" : 0, "Pacific" : 0}
for key, values in divisions.items():
    for value in values: 
        for attack_state in whites["state"]:
            if value == attack_state:
                whites_by_region[key] += 1





keys = list(blacks_by_region.keys())
values = list(blacks_by_region.values())
import csv
with open('blacks_by_region.csv', 'w') as f:
    writer = csv.writer(f)
    writer.writerows(zip(keys, values))
f.close() 
keys = list(jews_by_region.keys())
values = list(jews_by_region.values())
import csv
with open('jews_by_region.csv', 'w') as f:
    writer = csv.writer(f)
    writer.writerows(zip(keys, values))
f.close()
keys = list(whites_by_region.keys())
values = list(whites_by_region.values())
import csv
with open('whites_by_region.csv', 'w') as f:
    writer = csv.writer(f)
    writer.writerows(zip(keys, values))
f.close()