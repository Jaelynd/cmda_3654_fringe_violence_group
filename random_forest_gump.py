# -*- coding: utf-8 -*-
"""
Created on Sun Dec  2 19:37:44 2018

@author: Wylans xps 13
"""
import sklearn as sk

import pandas as pd
data = None
with open("./globalterrorism2.csv") as file:
    data = pd.read_csv(file)


gname = data["gname"]

gname = gname[~data["gname"].str.contains("unknown")]
y = [0] * len(gname)
i = 0
bad = 0
for x in gname:
    x = x.lower()
    if "white" in x or "ku" in x or "nazi" in x or "aryan" in x and "anti-white" not in x or "skin" in x:
        y[i] = "White_National"
    elif "eco" in x or "animal" in x or "green" in x or "environ" in x or "eart" in x or "healthy" in x or "tree" in x and not "anti-envir" in x or "csp" in x or "justice dep" in x:
        y[i] = "Eco-Friendly"
    elif "black" in x or "african" in x or "anti-w" in x or "zebra" in x:
        y[i] = "Black_National"
    elif "jew" in x:
        y[i] = "Jewish National"
    elif "priest" in x or "church" in x or "god" in x or "al-q" in x or "christian" in x:
        y[i] = "Religion" 
    elif "muslim" in x or "jiha" in x and "anti-mus" not in x or "qadd" in x or "mujah" in x:
        y[i] = "Religion"
    elif "abort" in x:
        y[i] = "Anti-Abortion"
    elif "student" in x:
        y[i] = "Student_Radicals"
    elif "left" in x or "armada" in x or "anti-gun" in x or "anar" in x or "comm" in x or "uff" in x or "revolut" in x or "george" in x or "fred" in x or "guerilla" in x or "anti-rep" in x or "anti-trum" in x:
        y[i] = "Left_Wing_Militants"
    elif "omega" in x or "macheteros" in x or "iran" in x or "armeni" in x or "liberati" in x or "puerto" in x or "grupo" in x or "cuba" in x or "croati" in x:
        y[i] = "Liberation Group"
    elif "MIRA" in x:
        y[i] = "MIRA"
    elif "weather" in x:
        y[i] = "Weathermen"
    elif "strike" in x:
        y[i] = "Strikers"
    elif "right-w" in x or "sovereign cit" in x or "compet" in x or "guerr" in x or "nati" in x or "anti-lg" in x or "anti-liber" in x:
        y[i] = "Right-Wing Nationalists"
    elif "anti-mus" in x or "anti-semit" in x or "anti-ara" in x:
        y[i] = "Racist Extremists"
    elif "anti-pol" in x or "anti-gov" in x:
        y[i] = "Anti-Government"
    else:
        if "unknown" not in x:
            bad += 1
            print(x)
        y[i] = "Other"
    i += 1


data["y"] = y
data.to_csv("rf.csv")
